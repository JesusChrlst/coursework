﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GameLib
{
    public class Bullet
    {
        //Передаём форму для удобного доступа
        public static Form GameForm = new Form();

        //Картинка пули и направление
        protected Image bulletDirecion;
        public Direction CurrentBulletDirection { get; set; }

        //Начальная позиция пули
        public int BulletLeft { get; set; }
        public int BulletTop { get; set; }

        //Скорость пули
        public int SpeedOfBullet { get; private set; }

        public PictureBox bullet = new PictureBox();
        public Timer BulletTimer = new Timer();

        //Разрушение пули(анимация)
        private PictureBox Detonation = new PictureBox();
        private Timer DestroyBulletTimer = new Timer();

        public Bullet()
        {
            SpeedOfBullet = 3;
            Detonation.Image = Images.Detonation;
        }

        public void MakeBullet()
        {
            //Направление пули(картинка)
            //-=хх для правильного спавна пули
            if (CurrentBulletDirection == Direction.Up)
            {
                BulletTop -= 25;
                bulletDirecion = Images.BulletUp;
            }
            else if (CurrentBulletDirection == Direction.Down)
            {
                BulletTop += 15;
                bulletDirecion = Images.BulletDown;
            }
            else if (CurrentBulletDirection == Direction.Left)
            {
                BulletLeft -= 25;
                bulletDirecion = Images.BulletLeft;
            }
            else
            {
                BulletLeft += 15;
                bulletDirecion = Images.BulletRight;
            }


            bullet.Image = bulletDirecion;
            bullet.Left = BulletLeft;
            bullet.Top = BulletTop;
            bullet.BackColor = Color.Transparent;

            GameForm.Controls.Add(bullet);

            BulletTimer.Interval = SpeedOfBullet;
            BulletTimer.Tick += new EventHandler(BulletTimerTick);
            BulletTimer.Start();
        }

        private void BulletTimerTick(object sender, EventArgs e)
        {
            //Полёт снаряда
            if (CurrentBulletDirection == Direction.Left)
            {
                bullet.Left -= SpeedOfBullet;
            }
            if (CurrentBulletDirection == Direction.Right)
            {
                bullet.Left += SpeedOfBullet;
            }
            if (CurrentBulletDirection == Direction.Up)
            {
                bullet.Top -= SpeedOfBullet;
            }
            if (CurrentBulletDirection == Direction.Down)
            {
                bullet.Top += SpeedOfBullet;
            }

            //Если выходит за пределы карты
            if (bullet.Left < 0 || bullet.Left > 520 || bullet.Top < 0 || bullet.Top > 520)
            {
                BulletTimer.Stop();
                BulletTimer.Dispose();
                BulletTimer = null;

                GameForm.Controls.Remove(bullet);
            }
        }

        public void DestroyBullet()
        {
            //Для правильного спавна взрыва
            Detonation.Left = bullet.Left - 20;
            Detonation.Top = bullet.Top - 20;
            Detonation.BackColor = Color.Transparent;

            GameForm.Controls.Add(Detonation);

            DestroyBulletTimer.Interval = 500;
            DestroyBulletTimer.Tick += new EventHandler(DestroyBulletTick);
            DestroyBulletTimer.Start();
        }

        //Таймер для исчезновения взрыва
        private void DestroyBulletTick(object sender, EventArgs e)
        {
            GameForm.Controls.Remove(Detonation);

            DestroyBulletTimer.Stop();
            DestroyBulletTimer.Dispose();
        }
    }
}
