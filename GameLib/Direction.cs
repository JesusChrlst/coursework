﻿namespace GameLib
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }
}
