﻿using System.IO;
using System.Drawing;

namespace GameLib
{
    public static class Images
    {
        //Пути к картинкам
        private static readonly string pathForTanks = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Textures\Tank\Sprites";
        private static readonly string pathForBullet = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Textures\Shell";
        private static readonly string pathForBlocks = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Textures\Blocks";
        private static readonly string pathForEagle = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Textures\Eagle";
        private static readonly string pathForDetonation = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Textures\Detonation";

        private static Image Validate(string path)
        {
            return Image.FromFile(path);
        }

        #region TankSprites
        public static Image PlayerSprite
        {
            get
            {
                return Validate(pathForTanks + @"\player.png");
            }
        }
        public static Image EnemySprite
        {
            get
            {
                return Validate(pathForTanks + @"\enemy.png");
            }
        }
        public static Image NewTankIncoming
        {
            get
            {
                return Validate(pathForTanks + @"\NewTank.gif");
            }
        }
        #endregion

        #region Bullet

        public static Image BulletUp
        {
            get
            {
                return Validate(pathForBullet + @"\ShellUp.png");
            }
        }
        public static Image BulletDown
        {
            get
            {
                return Validate(pathForBullet + @"\ShellDown.png");
            }
        }
        public static Image BulletLeft
        {
            get
            {
                return Validate(pathForBullet + @"\ShellLeft.png");
            }
        }
        public static Image BulletRight
        {
            get
            {
                return Validate(pathForBullet + @"\ShellRight.png");
            }
        }
        public static Image Detonation
        {
            get
            {
                return Validate(pathForDetonation + @"\Detonation.gif");
            }
        }

        #endregion

        #region Blocks
        public static Image BrickWall
        {
            get
            {
                return Validate(pathForBlocks + @"\BrickWall.png");
            }
        }
        public static Image ConcreteWall
        {
            get
            {
                return Validate(pathForBlocks + @"\ConcreteWall.png");
            }
        }
        public static Image Forest
        {
            get
            {
                return Validate(pathForBlocks + @"\Forest.png");
            }
        }

        #endregion

        #region Eagle
        public static Image Eagle
        {
            get
            {
                return Validate(pathForEagle + @"\Eagle.png");
            }
        }
        public static Image DefeatedEagle
        {
            get
            {
                return Validate(pathForEagle + @"\Eagle2.png");
            }
        }
        #endregion



    }
}
