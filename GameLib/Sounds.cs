﻿using System;
using System.IO;
using System.Windows.Media;

namespace GameLib
{
    public static class Sounds
    {
        private static readonly string pathForSounds = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Sounds";
        private static MediaPlayer media;

        private static MediaPlayer Validate(string path)
        {
            media = new MediaPlayer();
            media.Open(new Uri(path));
            return media;
        }

        #region BulletSound
        public static MediaPlayer StartBullet
        {
            get
            {
                return Validate(pathForSounds + @"\Fire.wav");
            }
        }

        public static MediaPlayer DestroyEnemy
        {
            get
            {
                return Validate(pathForSounds + @"\DetonationShellBig.wav");
            }
        }

        public static MediaPlayer DetonationBullet
        {
            get
            {
                return Validate(pathForSounds + @"\DetonationShell.wav");
            }
        }
        #endregion

        #region Tank
        public static MediaPlayer NewTank
        {
            get
            {
                return Validate(pathForSounds + @"\CountTankIncrement.wav");
            }
        }
        #endregion

        #region Game
        public static MediaPlayer StartLevel
        {
            get
            {
                return Validate(pathForSounds + @"\StartLevel.wav");
            }
        }

        public static MediaPlayer WON
        {
            get
            {
                return Validate(pathForSounds + @"\YOUWON.wav");
            }
        }

        public static MediaPlayer GAMEOVER
        {
            get
            {
                return Validate(pathForSounds + @"\GameOver.wav");
            }
        }
        #endregion
    }
}
