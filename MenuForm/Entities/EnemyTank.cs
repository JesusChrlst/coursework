﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MenuForm.GameSettings;
using static MenuForm.GameSettings.DefaultSetings;
using GameLib;
using MenuForm.WidnowsForms;

namespace MenuForm.Entities
{
    public static class EnemyTank
    {
        //Список врагов на форме
        public static List<Tank> Enemies = new List<Tank>();

        //Смена позиции респавна ботов
        private static int EnemyPosition = 1;

        //Анимация появления
        private static Timer TimerNewEnemyTank = new Timer();
        private static PictureBox NewTankImage = new PictureBox();

        //Добавляем вражеский танк
        public static void AddEnemyTank(Form form, Image enemySprite)
        {
            NewTankImage.Image = Images.NewTankIncoming;

            TimerNewEnemyTank.Interval = 600;
            TimerNewEnemyTank.Tick += new System.EventHandler(NewTankIncomingTick);
            switch (EnemyPosition)
            {
                case 1:
                    {
                        NewTankImage.Left = StartPositionTankEnemy1X;
                        NewTankImage.Top = StartPositionTankEnemy1Y;
                        StartAnimation();

                        Tank newTank = new Tank(StartPositionTankEnemy1X, StartPositionTankEnemy1Y, MoveFrames, enemySprite);
                        Enemies.Add(newTank);
                        MapClass.AllTanksOnMap.Add(newTank);
                        EnemyPosition++;
                        break;
                    }
                case 2:
                    {
                        NewTankImage.Left = StartPositionTankEnemy2X;
                        NewTankImage.Top = StartPositionTankEnemy2Y;
                        StartAnimation();

                        Tank newTank = new Tank(StartPositionTankEnemy2X, StartPositionTankEnemy2Y, MoveFrames, enemySprite);
                        Enemies.Add(newTank);
                        MapClass.AllTanksOnMap.Add(newTank);
                        EnemyPosition++;
                        break;
                    }
                case 3:
                    {
                        NewTankImage.Left = StartPositionTankEnemy3X;
                        NewTankImage.Top = StartPositionTankEnemy3Y;
                        StartAnimation();

                        Tank newTank = new Tank(StartPositionTankEnemy3X, StartPositionTankEnemy3Y, MoveFrames, enemySprite);
                        Enemies.Add(newTank);
                        MapClass.AllTanksOnMap.Add(newTank);
                        EnemyPosition = 1;
                        break;
                    }
            }
        }

        private static void StartAnimation()
        {
            TimerNewEnemyTank.Start();

            NewGameForm.ActiveForm.Controls.Add(NewTankImage);
            NewTankImage.BackColor = Color.Transparent;
        }

        private static void NewTankIncomingTick(object sender, System.EventArgs e)
        {
            NewGameForm.ActiveForm.Controls.Remove(NewTankImage);
            TimerNewEnemyTank.Stop();
            TimerNewEnemyTank.Dispose();
        }
    }
}
