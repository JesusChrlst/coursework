﻿using GameLib;
using MenuForm.GameSettings;
using System.Drawing;
using System.Windows.Forms;
using static MenuForm.GameSettings.DefaultSetings;

namespace MenuForm.Entities
{
    public class Tank
    {
        //Позиция танка
        public int PosX { get; set; }
        public int PosY { get; set; }

        //Фото и кол-во кадров
        private Image Image;
        private int MovementFrames;

        public int Size = DefaultSetings.HeightTank;

        //Движение танка
        public int DirX { get; set; }
        public int DirY { get; set; }
        public bool IsMoving { get; set; }

        //Анимация танка
        private int CurrentAnimation;
        private int CurrentFrame;
        private int CurrentLimit;

        //Поля для пули
        public Bullet TankBullet { get; set; }
        public bool IsShooting { get; set; }
        public Direction BulletDirection { get; set; }


        public Tank(int posX, int posY, int movementFrames, Image image)
        {
            this.PosX = posX;
            this.PosY = posY;
            this.MovementFrames = movementFrames;
            this.Image = image;
            CurrentAnimation = 0;
            CurrentFrame = 0;
            CurrentLimit = movementFrames;
            IsShooting = false;
            BulletDirection = Direction.Down;
        }

        public void Move()
        {
            PosX += DirX;
            PosY += DirY;
        }

        //Прорисовка кусочка спрайта для анимации
        public void PlayAnimation(Graphics g)
        {

            if (IsMoving == true)
            {
                //Перерисовываем анимацию
                if (CurrentFrame < CurrentLimit - 1)
                {
                    CurrentFrame++;
                }
                else
                {
                    CurrentFrame = 0;
                }
                //Указываем фото, позицию, размеры спрайта и отрисовку
                //Rectangle для определения размера на карте, за ним расположение и размеры на спрайте
                g.DrawImage(Image, new Rectangle(new Point(PosX, PosY), new Size(Size, Size)), WidthTank * CurrentFrame, HeightTank * CurrentAnimation, WidthTank, HeightTank, GraphicsUnit.Pixel);

            }
            else
            {
                g.DrawImage(Image, new Rectangle(new Point(PosX, PosY), new Size(Size, Size)), WidthTank * CurrentFrame, HeightTank * CurrentAnimation, WidthTank, HeightTank, GraphicsUnit.Pixel);
            }
        }

        public void SetAnitamiton(int currentAnimation)
        {
            //Просчитываем движения
            this.CurrentAnimation = currentAnimation;
            switch (currentAnimation)
            {
                //Down
                case 0:
                    {
                        CurrentLimit = MovementFrames;
                        break;
                    }
                //Up
                case 1:
                    {
                        CurrentLimit = MovementFrames;
                        break;
                    }
                //Left
                case 2:
                    {
                        CurrentLimit = MovementFrames;
                        break;
                    }
                //Right
                case 3:
                    {
                        CurrentLimit = MovementFrames;
                        break;
                    }
            }
        }


        public void RemoveTank(Tank removedtank)
        {
            if (removedtank.TankBullet.bullet != null)
            {
                Form.ActiveForm.Controls.Remove(removedtank.TankBullet.bullet);
            }
            EnemyTank.Enemies.Remove(removedtank);
            MapClass.AllTanksOnMap.Remove(removedtank);
        }

    }
}
