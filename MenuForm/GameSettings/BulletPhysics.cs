﻿using System;
using System.Drawing;
using MenuForm.Entities;

namespace MenuForm.GameSettings
{
    public static class BulletPhysics
    {
        //Если пуля пересекается с объектом
        public static bool BulletCollide(Tank tank)
        {
            //Выходл за пределы карты
            if (tank.TankBullet.bullet.Left < 0 || tank.TankBullet.bullet.Left > DefaultSetings.WidthBoard || tank.TankBullet.bullet.Top < 0 || tank.TankBullet.bullet.Top > DefaultSetings.HeightBoard)
            {
                return true;
            }

            for (int i = 0; i < MapClass.mapObjects.Count; i++)
            {
                var currentObject = MapClass.mapObjects[i];
                if (currentObject.Bounds.IntersectsWith(new Rectangle(new Point(tank.TankBullet.bullet.Left, tank.TankBullet.bullet.Top), new Size(DefaultSetings.WidthBullet, DefaultSetings.HeightBullet))))
                {
                    MapClass.DestroyBlock(MapClass.mapObjects[i]);
                    return true;
                }
            }

            return false;
        }

        //Если пуля пересекается с ботом
        public static bool BulledCollideEnemies(Tank tank)
        {
            Point delta = new Point();
            foreach (var tankObject in EnemyTank.Enemies)
            {
                //Приближение к танку
                delta.X = (tank.TankBullet.bullet.Left + DefaultSetings.WidthBullet / 2) - (tankObject.PosX + tankObject.Size / 2);
                delta.Y = (tank.TankBullet.bullet.Top + DefaultSetings.HeightBullet / 2) - (tankObject.PosY + tankObject.Size / 2);

                //Если приблизились
                if (Math.Abs(delta.X) <= DefaultSetings.WidthBullet / 2 + tankObject.Size)
                {
                    if (Math.Abs(delta.Y) <= DefaultSetings.HeightBullet / 2 + tankObject.Size)
                    {
                        //Вычисляем с какой стороны летит пуля
                        //Move right
                        if (delta.X <= 0 && tank.TankBullet.SpeedOfBullet == DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Top + DefaultSetings.WidthBullet > tankObject.PosY)
                        {
                            tankObject.RemoveTank(tankObject);
                            return true;
                        }
                        //Move left
                        if (delta.X > 0 && tank.TankBullet.SpeedOfBullet == -DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Top + DefaultSetings.WidthBullet > tankObject.PosY)
                        {
                            tankObject.RemoveTank(tankObject);
                            return true;
                        }
                        //Move down
                        if (delta.Y <= 0 && tank.TankBullet.SpeedOfBullet == DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Left + DefaultSetings.HeightBullet > tankObject.PosX)
                        {
                            tankObject.RemoveTank(tankObject);
                            return true;
                        }
                        //Move up
                        if (delta.Y > 0 && tank.TankBullet.SpeedOfBullet == -DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Left + DefaultSetings.HeightBullet > tankObject.PosX)
                        {
                            tankObject.RemoveTank(tankObject);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //Если пуля пересекается с игроком
        public static bool BulledCollidePlayer(Tank tank, Tank player)
        {
            Point delta = new Point();

            delta.X = (tank.TankBullet.bullet.Left + DefaultSetings.WidthBullet / 2) - (player.PosX + player.Size / 2);
            delta.Y = (tank.TankBullet.bullet.Top + DefaultSetings.HeightBullet / 2) - (player.PosY + player.Size / 2);

            if (Math.Abs(delta.X) <= DefaultSetings.WidthBullet / 2 + player.Size)
            {
                if (Math.Abs(delta.Y) <= DefaultSetings.HeightBullet / 2 + player.Size)
                {
                    //Move right
                    if (delta.X <= 0 && tank.TankBullet.SpeedOfBullet == DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Top + DefaultSetings.WidthBullet > player.PosY)
                    {
                        return true;
                    }
                    //Move left
                    if (delta.X > 0 && tank.TankBullet.SpeedOfBullet == -DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Top + DefaultSetings.WidthBullet > player.PosY)
                    {
                        return true;
                    }
                    //Move down
                    if (delta.Y <= 0 && tank.TankBullet.SpeedOfBullet == DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Left + DefaultSetings.HeightBullet > player.PosX)
                    {
                        return true;
                    }
                    //Move up
                    if (delta.Y > 0 && tank.TankBullet.SpeedOfBullet == -DefaultSetings.SpeedBullet && tank.TankBullet.bullet.Left + DefaultSetings.HeightBullet > player.PosX)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
