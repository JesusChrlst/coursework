﻿namespace MenuForm.GameSettings
{
    public class DefaultSetings
    {
        public static int MoveFrames = 2;

        //Стандартные настройки для игры
        static DefaultSetings()
        {
            WidthTitle = 20;
            HeightTitle = 20;

            HeightBoard = 26 * HeightTitle;
            WidthBoard = 26 * WidthTitle;

            HeightTank = 29;
            WidthTank = 29;

            TankSpeed = 5;

            WidthBullet = 5;
            HeightBullet = 5;
            SpeedBullet = 3;

            WidthEagle = 40;
            HeightEagle = 40;

            StartPlayerPositionX = 9 * WidthTitle;
            StartPlayerPositionY = HeightBoard - HeightTank;
            StartPositionTankEnemy1X = 1;
            StartPositionTankEnemy1Y = 1;
            StartPositionTankEnemy2X = 12 * WidthTitle;
            StartPositionTankEnemy2Y = 0;
            StartPositionTankEnemy3X = WidthBoard - WidthTank * 2;
            StartPositionTankEnemy3Y = 0;

            PositionEagleX = 12 * WidthTitle;
            PositionEagleY = HeightBoard - HeightTitle * 2;

        }

        public static int WidthTitle { get; private set; }
        public static int HeightTitle { get; private set; }
        public static int WidthTank { get; private set; }
        public static int HeightTank { get; private set; }
        public static int WidthBoard { get; private set; }
        public static int HeightBoard { get; private set; }
        public static int WidthBullet { get; private set; }
        public static int HeightBullet { get; private set; }
        public static int SpeedBullet { get; private set; }

        public static int StartPlayerPositionX { get; private set; }
        public static int StartPlayerPositionY { get; private set; }
        public static int StartPositionTankEnemy1X { get; private set; }
        public static int StartPositionTankEnemy1Y { get; private set; }
        public static int StartPositionTankEnemy2X { get; private set; }
        public static int StartPositionTankEnemy2Y { get; private set; }
        public static int StartPositionTankEnemy3X { get; private set; }
        public static int StartPositionTankEnemy3Y { get; private set; }

        public static int PositionEagleX { get; private set; }
        public static int PositionEagleY { get; private set; }
        public static int WidthEagle { get; private set; }
        public static int HeightEagle { get; private set; }

        public static int TankSpeed { get; private set; }

        //BrickWall
        public static char CharBrickWall { get { return '#'; } }

        //ConcerteWall
        public static char CharConcreteWall { get { return '@'; } }

        //Forest
        public static char CharForest { get { return '%'; } }
    }
}
