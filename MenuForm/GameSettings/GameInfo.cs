﻿namespace MenuForm.GameSettings
{
    public static class GameInfo
    {
        public static int Score { get; set; }
        public static int CurrentLevel { get; set; }
        public static int DefeatedEnemies { get; set; }
        static GameInfo()
        {
            CurrentLevel = 1;
            Score = 0;
            DefeatedEnemies = 0;
        }
    }
}
