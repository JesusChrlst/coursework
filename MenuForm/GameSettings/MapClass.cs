﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using GameLib;
using static MenuForm.GameSettings.DefaultSetings;
using System.Windows.Forms;
using MenuForm.WidnowsForms;
using MenuForm.Entities;

namespace MenuForm.GameSettings
{
    public static class MapClass
    {
        //Карта и объекты на карте
        public static PictureBox[,] map = new PictureBox[30, 30];
        public static List<PictureBox> mapObjects;

        public static List<Tank> AllTanksOnMap = new List<Tank>();

        public static void GenerateMap(Form form)
        {
            mapObjects = new List<PictureBox>();
            //Находим путь к файлу
            string[] linesTileMap = File.ReadAllLines(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Maps\" + GameInfo.CurrentLevel.ToString());

            //Проходимся по строкам. Каждый символ - какой-то блок, который добавляем на форму
            int i = 0;
            foreach (string line in linesTileMap)
            {
                int j = 0;
                foreach (char c in line)
                {
                    //Каждый символ - какой-то блок на карте
                    if (c == DefaultSetings.CharBrickWall)
                    {
                        AddPictureBox(map, i, j);
                        map[i, j].Name = "Brick";
                        map[i, j].Image = Images.BrickWall;
                        form.Controls.Add(map[i, j]);
                        AddBlock(map, i, j);
                    }
                    if (c == DefaultSetings.CharConcreteWall)
                    {
                        AddPictureBox(map, i, j);
                        map[i, j].Name = "Concrete";
                        map[i, j].Image = Images.ConcreteWall;
                        form.Controls.Add(map[i, j]);
                        AddBlock(map, i, j);
                    }
                    if (c == DefaultSetings.CharForest)
                    {
                        AddPictureBox(map, i, j);
                        map[i, j].Name = "Forest";
                        map[i, j].Image = Images.Forest;
                        map[i, j].BackColor = Color.Transparent;
                        form.Controls.Add(map[i, j]);
                    }
                    if (i == 25 && j == 13)
                    {
                        map[i, j] = new PictureBox();
                        map[i, j].Image = Images.Eagle;
                        map[i, j].Name = "Eagle";
                        map[i, j].Width = DefaultSetings.WidthEagle;
                        map[i, j].Height = DefaultSetings.HeightEagle;
                        map[i, j].Left = DefaultSetings.PositionEagleX;
                        map[i, j].Top = DefaultSetings.PositionEagleY;
                        form.Controls.Add(map[i, j]);
                        AddBlock(map, i, j);
                    }
                    j++;
                }
                i++;
            }
        }

        public static void AddPictureBox(PictureBox[,] array, int i, int j)
        {
            array[i, j] = new PictureBox();
            array[i, j].Width = DefaultSetings.WidthTitle;
            array[i, j].Height = DefaultSetings.HeightTitle;
            array[i, j].Left = j * WidthTitle;
            array[i, j].Top = i * HeightTitle;
        }
        public static void AddBlock(PictureBox[,] array, int i, int j)
        {
            //Добавляем каждый блок в список
            mapObjects.Add(array[i, j]);
        }


        public static void DestroyBlock(PictureBox destroyedBlock)
        {
            if (destroyedBlock.Name == "Eagle") 
            {
                destroyedBlock.Image = Images.DefeatedEagle;
                NewGameForm form = new NewGameForm();
                Task.Run(() =>
                {
                    NewGameForm.ActiveForm.Invoke((MethodInvoker)delegate
                    {
                        form.GAMEOVER();
                    });
                });
            }
            else if (destroyedBlock.Name == "Brick")
            {
                Task.Run(() =>
                {
                    NewGameForm.ActiveForm.Invoke((MethodInvoker)delegate
                    {
                        RemoveBlock(destroyedBlock);
                    });
                });
            }
        }
        private static void RemoveBlock(PictureBox blockForRemove)
        {
            NewGameForm.ActiveForm.Controls.Remove(blockForRemove);
            mapObjects.Remove(blockForRemove);
        }
    }
}
