﻿using System;
using System.Drawing;
using MenuForm.Entities;

namespace MenuForm.GameSettings
{
    public static class Physics
    {
        //Коллизия для танков
        public static bool IsCollide(Tank tank, Point dir)
        {
            PointF delta = new PointF();
            
            //Коллизия края карты
            if (tank.PosX + dir.X < 0 || tank.PosX + tank.Size + dir.X > DefaultSetings.WidthBoard || tank.PosY + dir.Y < 0 || tank.PosY + tank.Size + dir.Y > DefaultSetings.HeightBoard)
            {
                return true;
            }
            //Для коллизии объектов
            for (int i = 0; i < MapClass.mapObjects.Count; i++)
            {
                var currentObject = MapClass.mapObjects[i];
                //Расстояние до объекта по X и Y
                delta.X = (tank.PosX + tank.Size / 2) - (currentObject.Left + currentObject.Size.Width / 2);
                delta.Y = (tank.PosY + tank.Size / 2) - (currentObject.Top + currentObject.Size.Height / 2);
                //+2 и +3 чтобы не доезжать до блока
                if (Math.Abs(delta.X) <= tank.Size / 2 + currentObject.Size.Width / 2 + 2)
                {
                    if (Math.Abs(delta.Y) <= tank.Size / 2 + currentObject.Size.Height / 2 + 3)
                    {
                        //-3 чтобы танк не въезжал в блок
                        //Move right
                        if (delta.X <= 0 && dir.X == DefaultSetings.TankSpeed && tank.PosY + tank.Size - 3 > currentObject.Top)
                        {
                            return true;
                        }
                        //Move left
                        if (delta.X > 0 && dir.X == -DefaultSetings.TankSpeed && tank.PosY + tank.Size - 3 > currentObject.Top)
                        {
                            tank.PosX += 1;
                            return true;
                        }
                        //Move down
                        if (delta.Y <= 0 && dir.Y == DefaultSetings.TankSpeed && tank.PosX + tank.Size - 3 > currentObject.Left)
                        {
                            return true;
                        }
                        //Move up
                        if (delta.Y > 0 && dir.Y == -DefaultSetings.TankSpeed && tank.PosX + tank.Size - 3 > currentObject.Left)
                        {
                            tank.PosY += 1;
                            return true;
                        }
                    }
                }
            }

            //Для коллизии танков
            Tank tempTank = tank;
            MapClass.AllTanksOnMap.Remove(tempTank);
            foreach (var tankObject in MapClass.AllTanksOnMap)
            {
                delta.X = (tank.PosX + tank.Size / 2) - (tankObject.PosX + tankObject.Size / 2);
                delta.Y = (tank.PosY + tank.Size / 2) - (tankObject.PosY + tankObject.Size / 2);

                if (Math.Abs(delta.X) <= tank.Size / 2 + tankObject.Size / 2)
                {
                    if (Math.Abs(delta.Y) <= tank.Size / 2 + tankObject.Size / 2 + 3)
                    {
                        //Move right
                        if (delta.X <= 0 && dir.X == DefaultSetings.TankSpeed && tank.PosY + tank.Size > tankObject.PosY)
                        {
                            if (CheckTankCount())
                            {
                                MapClass.AllTanksOnMap.Add(tempTank);
                            }
                            return true;
                        }
                        //Move left
                        if (delta.X > 0 && dir.X == -DefaultSetings.TankSpeed && tank.PosY + tank.Size > tankObject.PosY)
                        {
                            if (CheckTankCount())
                            {
                                MapClass.AllTanksOnMap.Add(tempTank);
                            }
                            tank.PosX += 1;
                            return true;
                        }
                        //Move down
                        if (delta.Y <= 0 && dir.Y == DefaultSetings.TankSpeed && tank.PosX + tank.Size > tankObject.PosX)
                        {
                            if (CheckTankCount())
                            {
                                MapClass.AllTanksOnMap.Add(tempTank);
                            }
                            return true;
                        }
                        //Move up
                        if (delta.Y > 0 && dir.Y == -DefaultSetings.TankSpeed && tank.PosX + tank.Size > tankObject.PosX)
                        {
                            if (CheckTankCount())
                            {
                                MapClass.AllTanksOnMap.Add(tempTank);
                            }
                            tank.PosY += 1;
                            return true;
                        }
                    }
                }
                if (tank.PosX == tankObject.PosX && tank.PosY == tankObject.PosY)
                {
                    if (CheckTankCount())
                    {
                        MapClass.AllTanksOnMap.Add(tempTank);
                    }
                    return false;
                }
            }
            if (CheckTankCount())
            {
                MapClass.AllTanksOnMap.Add(tempTank);
            }
            return false;
        }
        private static bool CheckTankCount()
        {
            if (MapClass.AllTanksOnMap.Count != 3)
            {
                return true;
            }
            return false;
        }
    }
}
