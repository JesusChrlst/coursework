﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Windows.Forms;
using MenuForm.WidnowsForms;

namespace MenuForm
{
    public partial class MainMenuForm : Form
    {
        private bool NextLevel;
        public MainMenuForm(bool nextLevel)
        {
            NextLevel = nextLevel;
            InitializeComponent();
        }

        private void MenuForm_Load(object sender, EventArgs e)
        {
            //Если переходим на следующий уровень то сразу запускаем его
            if (NextLevel)
            {
                buttonPlay.PerformClick();
            }

            PrivateFontCollection fonts = new PrivateFontCollection();
            fonts.AddFontFile(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\Content\Font\BattleCityInfo.ttf".ToString());
            fonts.AddFontFile(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\Content\Font\BattleCity.ttf".ToString());

            Font battleFont = new Font(fonts.Families[0], 55);
            Font infoFont = new Font(fonts.Families[1], 12, FontStyle.Regular);

            labelBattle.Font = battleFont;
            labelCity.Font = battleFont;
            buttonPlay.Font = infoFont;
            buttonExit.Font = infoFont;
            labelCompany.Font = infoFont;
            labelRights.Font = infoFont;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            LoadingLevelForm loading = new LoadingLevelForm();
            NewGameForm game = new NewGameForm();
            Close();
            //После прогрузки вызывается игра
            if (loading.ShowDialog() == DialogResult.OK)
            {
                game.Refresh();
                game.ShowDialog();
            }
        }
    }
}
