﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace MenuForm.Models
{
    class Images
    {
        private static readonly string pathForBullet = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName.ToString() + @"\Content\Textures\Shell";
        private static Image bulletUp;

        private static Image Validate(Image img, string path)
        {
            return img = new Bitmap(path);
        }

        public static Image BulletUp
        {
            get
            {
                return Validate(bulletUp, pathForBullet + @"ShellUp.png");
            }
        }
    }
}
