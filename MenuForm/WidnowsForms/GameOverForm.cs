﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Windows.Forms;
using MenuForm.GameSettings;
using GameLib;

namespace MenuForm.WidnowsForms
{
    public partial class GameOverForm : Form
    {
        public GameOverForm()
        {
            InitializeComponent();

            PrivateFontCollection fonts = new PrivateFontCollection();
            fonts.AddFontFile(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\Content\Font\BattleCityInfo.ttf".ToString());
            fonts.AddFontFile(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\Content\Font\BattleCity.ttf".ToString());

            Font battleFont = new Font(fonts.Families[0], 55);
            Font infoFont = new Font(fonts.Families[1], 12, FontStyle.Regular);

            labelGameOver.Font = battleFont;
            labelFinalScore.Font = infoFont;
            labelFinalScore.Text += GameInfo.Score.ToString();

            if (GameInfo.CurrentLevel == 9)
            {
                Sounds.WON.Play();

                labelGameOver.Text = "YOU\nWON!";

                timerChangeColor.Interval = 100;
                timerChangeColor.Tick += new EventHandler(ChangeColorTick);

                timerGameOver.Interval = 8500;

                timerChangeColor.Start();
            }
            timerGameOver.Tick += new EventHandler(GameOverTick);

            timerGameOver.Start();
        }
        private void ChangeColorTick(object sender, EventArgs e)
        {
            if (labelGameOver.ForeColor == Color.White)
            {
                labelGameOver.ForeColor = Color.Yellow;
            }
            else if (labelGameOver.ForeColor == Color.Yellow)
            {
                labelGameOver.ForeColor = Color.Blue;
            }
            else
            {
                labelGameOver.ForeColor = Color.White;
            }
        }
        private void GameOverTick(object sender, EventArgs e)
        {
            timerGameOver.Stop();
            Application.Exit();
        }
        private void GameOverForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();

        }
    }
}
