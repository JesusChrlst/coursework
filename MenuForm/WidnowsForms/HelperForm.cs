﻿using System;
using System.Windows.Forms;

namespace MenuForm.WidnowsForms
{
    //Она сделана главным окном для корректной работы переходов между уровней
    //и проверки на уровень
    public partial class HelperForm : Form
    {
        public bool NextLevel { get; set; }
        public HelperForm()
        {
            InitializeComponent();
        }

        private void HelperForm_Load(object sender, EventArgs e)
        {
            Hide();
            StartMenu();
        }

        public void StartMenu()
        {
            if (GameSettings.GameInfo.CurrentLevel != 9)
            {
                var menu = new MainMenuForm(NextLevel);
                menu.FormClosed += HelperForm_FormClosed;
                menu.Show();
            }
            else
            {
                GameOverForm CONGRATULATIONS = new GameOverForm();
                CONGRATULATIONS.ShowDialog();
            }
        }

        private void HelperForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Hide();
        }
    }
}
