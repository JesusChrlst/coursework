﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Windows.Forms;
using MenuForm.GameSettings;
using GameLib;

namespace MenuForm.WidnowsForms
{
    public partial class LoadingLevelForm : Form
    {
        public LoadingLevelForm()
        {
            InitializeComponent();
            PrivateFontCollection fonts = new PrivateFontCollection();
            fonts.AddFontFile(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\Content\Font\BattleCityInfo.ttf".ToString());
            fonts.AddFontFile(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\Content\Font\BattleCity.ttf".ToString());

            Font battleFont = new Font(fonts.Families[0], 55);
            Font infoFont = new Font(fonts.Families[1], 12, FontStyle.Regular);

            labelStage.Font = infoFont;
            labelStage.Text = "STAGE " + GameInfo.CurrentLevel.ToString();
            labelScore.Font = infoFont;
            labelScore.Text = "Your Score: " + GameInfo.Score.ToString();

            Sounds.StartLevel.Play();

            timerStartNextLevel.Interval = 5000;
            timerStartNextLevel.Tick += new EventHandler(SoundTick);
            timerStartNextLevel.Start();
        }

        private void SoundTick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            timerStartNextLevel.Stop();
            Close();
        }
    }
}
