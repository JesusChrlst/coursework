﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using MenuForm.Entities;
using MenuForm.GameSettings;
using static MenuForm.GameSettings.DefaultSetings;
using GameLib;
using System.Threading;

namespace MenuForm.WidnowsForms
{
    public partial class NewGameForm : Form
    {
        //Для корректной отрисовки анимаций
        private bool GameInProgress;
        protected Random rnd = new Random();

        //Спрайты
        protected Image PlayerSprite;
        protected Image EnemySprite;

        //Объекты танков
        protected Tank Player;

        //Счётчики
        protected int MaxCountEnemiesOnForm = 3;
        protected int EnemiesOnForm;

        public List<System.Windows.Forms.Timer> EnemyTimers = new List<System.Windows.Forms.Timer>();

        public NewGameForm()
        {
            EnemiesOnForm = 0;
            GameInProgress = true;

            InitializeComponent();

            //Игровой счетчик
            timerGame.Interval = 100;
            timerGame.Tick += new EventHandler(UpdateTick);

            //Счётчик для движения ботов
            for (int i = 0; i < MaxCountEnemiesOnForm; i++)
            {
                System.Windows.Forms.Timer newTimer = new System.Windows.Forms.Timer();
                newTimer.Interval = (rnd.Next(1000, 6000) / 1000) * 1000;
                newTimer.Tick += new EventHandler(EnemyTick);
                newTimer.Start();
                EnemyTimers.Add(newTimer);
            }

            //Когда нажата клавиша
            KeyDown += new KeyEventHandler(OnPress);

            Init();
        }
        public void Init()
        {
            //Выгружаем спрайты
            PlayerSprite = Images.PlayerSprite;
            EnemySprite = Images.EnemySprite;

            //Создаём игрока
            Player = new Tank(StartPlayerPositionX, StartPlayerPositionY, DefaultSetings.MoveFrames, PlayerSprite);
            MapClass.AllTanksOnMap.Add(Player);

            //Передаём форму
            Bullet.GameForm = this;

            timerGame.Start();

            DrawMap();
        }

        //Проверка коллизий, прорисовка и переход на след.уровень
        #region Collision
        public void UpdateTick(object sender, EventArgs e)
        {

            //Проверка на коллизию при движении
            if (Player.IsMoving)
            {
                //Передаём направление танка чтобы заранее знать о столкновении с объектом
                if (Physics.IsCollide(Player, new Point(Player.DirX, Player.DirY)) == false)
                {
                    Player.Move();
                }
            }

            foreach (var enemy in EnemyTank.Enemies)
            {
                if (enemy.IsMoving)
                {
                    if (Physics.IsCollide(enemy, new Point(enemy.DirX, enemy.DirY)) == false)
                    {
                        enemy.Move();
                    }
                }
            }

            //Враги при стрельбе
            foreach (var enemy in EnemyTank.Enemies)
            {
                if (enemy.IsShooting == true)
                {
                    if (BulletPhysics.BulletCollide(enemy) == true)
                    {
                        enemy.TankBullet.DestroyBullet();
                        this.Controls.Remove(enemy.TankBullet.bullet);
                        enemy.IsShooting = false;
                    }
                    if (BulletPhysics.BulledCollidePlayer(enemy, Player) == true)
                    {
                        enemy.TankBullet.DestroyBullet();
                        this.Controls.Remove(enemy.TankBullet.bullet);
                        enemy.IsShooting = false;
                        GAMEOVER();
                    }
                }
                //Если бот не стреляет, вызываем функцию стрельбы
                else if (enemy.IsShooting == false)
                {
                    enemy.TankBullet = new Bullet();
                    ShootBullet(enemy);
                }
            }

            //Игрок при стрельбе
            if (Player.IsShooting == true)
            {
                Thread thread = new Thread(() =>
                {
                    if (BulletPhysics.BulletCollide(Player) == true)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            Player.TankBullet.DestroyBullet();
                            this.Controls.Remove(Player.TankBullet.bullet);
                        });
                        Player.IsShooting = false;
                        Task.Run(() =>
                        {
                            Sounds.DetonationBullet.Play();
                        });
                    }
                    if (BulletPhysics.BulledCollideEnemies(Player) == true)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            Player.TankBullet.DestroyBullet();
                            this.Controls.Remove(Player.TankBullet.bullet);
                            AddScore();
                        });
                        Player.IsShooting = false;
                        Task.Run(() =>
                        {
                            Sounds.DestroyEnemy.Play();
                        });
                    }
                });
                thread.IsBackground = true;
                thread.Start();
            }

            //Переход на новый уровень
            if (GameInfo.DefeatedEnemies == 4)
            {
                NextLevel();
            }

            Invalidate();
        }
        #endregion

        //Прорисовка анимаций танков
        #region Animation
        private void OnPaint(object sender, PaintEventArgs e)
        {
            if (GameInProgress)
            {
                //Прорисовка анимаций танков
                Graphics g = e.Graphics;

                Player.PlayAnimation(g);

                if (EnemiesOnForm < MaxCountEnemiesOnForm && GameInfo.DefeatedEnemies <= 1)
                {
                    EnemiesOnForm++;
                    EnemyTank.AddEnemyTank(this, EnemySprite);
                    Task.Run(() =>
                    {
                        Sounds.NewTank.Play();
                    });
                }
                for (int i = 0; i < EnemyTank.Enemies.Count; i++)
                {
                    EnemyTank.Enemies[i].PlayAnimation(g);
                }

            }
        }
        #endregion

        //Движение и анимации танков
        #region Direction
        private void EnemyTick(object sender, EventArgs e)
        {
            foreach (var enemy in EnemyTank.Enemies)
            {
                enemy.DirX = 0;
                enemy.DirY = 0;
                switch (rnd.Next(0, 4))
                {
                    //up
                    case 0:
                        {
                            enemy.DirY = -TankSpeed;
                            enemy.IsMoving = true;
                            enemy.SetAnitamiton(1);
                            enemy.BulletDirection = Direction.Up;
                            break;
                        }
                    //down
                    case 1:
                        {
                            enemy.DirY = TankSpeed;
                            enemy.IsMoving = true;
                            enemy.SetAnitamiton(0);
                            enemy.BulletDirection = Direction.Down;
                            break;
                        }
                    //left
                    case 2:
                        {
                            enemy.DirX = -TankSpeed;
                            enemy.IsMoving = true;
                            enemy.SetAnitamiton(2);
                            enemy.BulletDirection = Direction.Left;
                            break;
                        }
                    //right
                    case 3:
                        {
                            enemy.DirX = TankSpeed;
                            enemy.IsMoving = true;
                            enemy.SetAnitamiton(3);
                            enemy.BulletDirection = Direction.Right;
                            break;
                        }
                }

            }
        }
        private void OnPress(object sender, KeyEventArgs e)
        {
            //Движение и стрельба игрока
            switch (e.KeyCode)
            {
                case Keys.Up:
                    {
                        Player.DirY = -TankSpeed;
                        Player.IsMoving = true;
                        Player.SetAnitamiton(1);
                        Player.BulletDirection = Direction.Up;
                        break;
                    }
                case Keys.Down:
                    {
                        Player.DirY = TankSpeed;
                        Player.IsMoving = true;
                        Player.SetAnitamiton(0);
                        Player.BulletDirection = Direction.Down;
                        break;
                    }
                case Keys.Left:
                    {
                        Player.DirX = -TankSpeed;
                        Player.IsMoving = true;
                        Player.SetAnitamiton(2);
                        Player.BulletDirection = Direction.Left;
                        break;
                    }
                case Keys.Right:
                    {
                        Player.DirX = TankSpeed;
                        Player.IsMoving = true;
                        Player.SetAnitamiton(3);
                        Player.BulletDirection = Direction.Right;
                        break;
                    }
                case Keys.Space:
                    {
                        if (Player.IsShooting == false)
                        {
                            Player.TankBullet = new Bullet();
                            Task.Run(() =>
                            {
                                Sounds.StartBullet.Play();
                            });
                            ShootBullet(Player);
                        }
                        break;
                    }
            }
        }

        private void NewGameForm_KeyUp(object sender, KeyEventArgs e)
        {
            //При отжатии клавиши(для плавной анимации)
            switch (e.KeyCode)
            {
                case Keys.Up:
                    {
                        Player.IsMoving = false;
                        Player.DirY = 0;
                        break;
                    }
                case Keys.Down:
                    {
                        Player.IsMoving = false;
                        Player.DirY = 0;
                        break;
                    }
                case Keys.Left:
                    {
                        Player.IsMoving = false;
                        Player.DirX = 0;
                        break;
                    }
                case Keys.Right:
                    {
                        Player.IsMoving = false;
                        Player.DirX = 0;
                        break;
                    }
            }
        }
        #endregion

        private void ShootBullet(Tank tank)
        {
            tank.TankBullet.CurrentBulletDirection = tank.BulletDirection;
            tank.TankBullet.BulletLeft = tank.PosX + (DefaultSetings.WidthTank / 2);
            tank.TankBullet.BulletTop = tank.PosY + (DefaultSetings.HeightTank / 2);
            tank.IsShooting = true;
            tank.TankBullet.MakeBullet();
        }
        private void NewGameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void DrawMap()
        {
            MapClass.GenerateMap(this);
        }
        private void AddScore()
        {
            GameInfo.Score += 100;
            GameInfo.DefeatedEnemies++;
            EnemiesOnForm--;
        }

        #region NextLevel or GameOver
        public void StopAllTimers()
        {
            timerGame.Stop();
            timerGame.Dispose();
            foreach (var enemyTimer in EnemyTimers)
            {
                enemyTimer.Stop();
                enemyTimer.Dispose();
            }
            foreach (var tank in MapClass.AllTanksOnMap)
            {
                tank.IsMoving = false;
                tank.IsShooting = false;
                if (tank.TankBullet != null)
                {
                    if (tank.TankBullet.BulletTimer != null)
                    {
                        tank.TankBullet.BulletTimer.Stop();
                        tank.TankBullet.BulletTimer.Dispose();
                    }
                }
            }
        }

        private void UpdateAllStats()
        {
            for (int i = 0; i != EnemyTank.Enemies.Count;)
            {
                EnemyTank.Enemies[i].RemoveTank(EnemyTank.Enemies[i]);
            }
            foreach (var tank in MapClass.AllTanksOnMap)
            {
                tank.TankBullet.bullet.Left = 590;
            }
            GameInfo.DefeatedEnemies = 0;
            GameInfo.CurrentLevel++;
            MapClass.AllTanksOnMap.Clear();
            MapClass.mapObjects.Clear();
        }

        private void NextLevel()
        {
            GameInProgress = false;
            StopAllTimers();
            UpdateAllStats();
            HelperForm form = new HelperForm();
            Hide();
            form.NextLevel = true;
            form.StartMenu();
        }

        public void GAMEOVER()
        {
            StopAllTimers();
            Sounds.GAMEOVER.Play();
            this.Hide();
            GameOverForm gameOver = new GameOverForm();
            gameOver.ShowDialog();
        }
        #endregion
    }
}
